package com.quhaad.project.statistic.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.quhaad.project.statistic.core.AnswerStat;
import java.util.HashMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

public class AnswerStatRepositoryTest {
    @InjectMocks
    private AnswerStatRepository answerStatRepository;
    private AnswerStatRepository answerStatRepositorySpy;

    @BeforeEach
    public void setUp() {
        answerStatRepository = new AnswerStatRepository();
        answerStatRepositorySpy = spy(answerStatRepository);
    }

    @Test
    public void testMethodGetAnswerStatMap() {
        HashMap<String, AnswerStat> answerMap = new HashMap<>();
        when(answerStatRepositorySpy.getAnswerStatMap()).thenReturn(answerMap);
        assertEquals(answerMap, answerStatRepositorySpy.getAnswerStatMap());
    }

    @Test
    public void testMethodAddAnswerStatByRoomId() {
        HashMap<String, AnswerStat> answerStatMap = spy(answerStatRepositorySpy.getAnswerStatMap());
        when(answerStatRepositorySpy.getAnswerStatMap()).thenReturn(answerStatMap);
        answerStatRepositorySpy.addAnswerStatByRoomId("roomId");
        verify(answerStatMap, times(1)).put(eq("roomId"), any(AnswerStat.class));
        assertEquals(1, answerStatMap.size());
    }

    @Test
    public void testMethodGetAnswerStatByRoomId() {
        HashMap<String, AnswerStat> answerStatMap = new HashMap<>();
        AnswerStat answerStat = new AnswerStat();
        answerStatMap.put("roomId", answerStat);
        HashMap<String, AnswerStat> answerStatMapSpy = spy(answerStatMap);

        when(answerStatRepositorySpy.getAnswerStatMap()).thenReturn(answerStatMapSpy);
        when(answerStatMapSpy.get("roomId")).thenReturn(answerStat);

        AnswerStat calledAnswerStat = answerStatRepositorySpy.getAnswerStatByRoomId("roomId");
        assertEquals(answerStat, calledAnswerStat);
        verify(answerStatMapSpy, times(1)).get("roomId");
    }

    @Test
    public void testMethodDeleteAnswerStatByRoomId() {
        HashMap<String, AnswerStat> answerStatMap = new HashMap<>();
        AnswerStat scoreboard = new AnswerStat();
        answerStatMap.put("roomId", scoreboard);
        HashMap<String, AnswerStat> answerStatMapSpy = spy(answerStatMap);

        when(answerStatRepositorySpy.getAnswerStatMap()).thenReturn(answerStatMapSpy);

        assertEquals(1, answerStatMapSpy.size());
        answerStatRepositorySpy.deleteAnswerStatByRoomId("roomId");
        assertTrue(answerStatMapSpy.isEmpty());
        verify(answerStatMapSpy, times(1)).remove("roomId");
    }

    @Test
    public void testMethodResetAnswerShouldCallGet() {
        HashMap<String, AnswerStat> answerStatMap = new HashMap<>();
        AnswerStat answerStat = new AnswerStat();
        answerStatMap.put("roomId", answerStat);
        HashMap<String, AnswerStat> answerStatMapSpy = spy(answerStatMap);

        when(answerStatRepositorySpy.getAnswerStatMap()).thenReturn(answerStatMapSpy);
        when(answerStatMapSpy.get("roomId")).thenReturn(answerStat);
        answerStatRepositorySpy.resetAnswerStatByRoomId("roomId");

        verify(answerStatMapSpy, times(1)).get("roomId");
    }


}
