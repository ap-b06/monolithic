package com.quhaad.project.scoreboard.core;

import com.quhaad.project.game.model.Player;
import java.util.PriorityQueue;

public class Scoreboard {

    private PriorityQueue<Player> pq = new PriorityQueue<>();

    public void addPlayer(Player player) {
        getPq().add(player);
    }

    public void updatePlayer(Player player) {
        getPq().remove(player);
        getPq().add(player);
    }

    public PriorityQueue<Player> getPq() {
        return pq;
    }

    public void setPq(PriorityQueue<Player> pq) {
        this.pq = pq;
    }
}
