package com.quhaad.project.question.repository;

import com.quhaad.project.question.core.QuestionSet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface QuestionSetRepository extends JpaRepository<QuestionSet, Integer> {
    String selectQuery = "SELECT q from QuestionSet q ";
    String finalQuery =  selectQuery + "WHERE q.questionSetId = :questionSetId";
    @Query(value = finalQuery)
    QuestionSet findById(@Param("questionSetId") int questionSetId);
}
