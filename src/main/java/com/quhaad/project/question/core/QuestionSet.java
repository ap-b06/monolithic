package com.quhaad.project.question.core;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "questionSet")
public class QuestionSet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int questionSetId;

    @Column(name = "questionSetName", nullable = false)
    private String questionSetName;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name = "question",
            joinColumns = @JoinColumn(name = "questionSetId")
    )
    private List<Question> listOfQuestions = new ArrayList<>();

    public int getQuestionSetId() {
        return questionSetId;
    }

    public void setQuestionSetId(int questionSetId) {
        this.questionSetId = questionSetId;
    }

    public String getQuestionSetName() {
        return questionSetName;
    }

    public void setQuestionSetName(String questionSetName) {
        this.questionSetName = questionSetName;
    }

    public List<Question> getListOfQuestions() {
        return listOfQuestions;
    }

    public void setListOfQuestions(List<Question> listOfQuestions) {
        this.listOfQuestions = listOfQuestions;
    }
}
