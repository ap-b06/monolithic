package com.quhaad.project.scoreboard.service;

import com.quhaad.project.scoreboard.core.Scoreboard;
import com.quhaad.project.scoreboard.repository.ScoreboardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScoreboardServiceImpl implements ScoreboardService {

    @Autowired
    private ScoreboardRepository scoreboardRepository;

    @Override
    public Scoreboard getScoreBoard(String roomId) {
        return getScoreboardRepository().getScoreboardByRoomId(roomId);
    }

    @Override
    public void addScoreBoard(String roomId) {
        getScoreboardRepository().addScoreboardByRoomId(roomId);
    }

    @Override
    public void deleteScoreBoard(String roomId) {
        getScoreboardRepository().deleteScoreboardByRoomId(roomId);
    }

    public ScoreboardRepository getScoreboardRepository() {
        return scoreboardRepository;
    }
}
