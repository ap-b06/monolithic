package com.quhaad.project.game.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.quhaad.project.game.model.Player;
import com.quhaad.project.game.model.Room;
import com.quhaad.project.game.repository.RoomRepository;
import com.quhaad.project.game.repository.UserRepository;
import com.quhaad.project.question.core.Question;
import com.quhaad.project.question.core.QuestionSet;
import com.quhaad.project.question.service.QuestionService;
import com.quhaad.project.scoreboard.core.Scoreboard;
import com.quhaad.project.scoreboard.service.ScoreboardService;
import com.quhaad.project.statistic.core.AnswerStat;
import com.quhaad.project.statistic.service.StatisticService;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith({MockitoExtension.class})
public class GameServiceImplTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private RoomRepository roomRepository;

    @Mock
    private ScoreboardService scoreboardService;

    @Mock
    private StatisticService statisticService;

    @Mock
    private QuestionService questionService;

    @InjectMocks
    private GameServiceImpl gameService;

    @Test
    public void testWhenEnteringGameButTheRoomIsFull() {
        Room room = new Room("roomFull");
        room.addPlayer(new Player("player1"));
        room.addPlayer(new Player("player2"));
        room.addPlayer(new Player("player3"));
        room.addPlayer(new Player("player4"));
        when(roomRepository.findByRoomId("roomFull")).thenReturn(room);
        assertEquals("ROOM_FULL", gameService.enterGame("player5", "roomFull"));
    }

    @Test
    public void testWhenEnteringGameButTheRoomIsNotFull() {
        Room room = new Room("roomFull");
        room.addPlayer(new Player("player1"));
        room.addPlayer(new Player("player2"));
        when(roomRepository.findByRoomId("roomFull")).thenReturn(room);
        Scoreboard scoreboard = new Scoreboard();
        when(scoreboardService.getScoreBoard("roomFull")).thenReturn(scoreboard);
        assertEquals("SUCCESS", gameService.enterGame("player3", "roomFull"));
    }

    @Test
    public void testWhenUsernameIsExistItShouldReturnRightResponse() {
        when(userRepository.checkIfUsernameExist("player")).thenReturn(true);
        assertEquals(true, gameService.checkIfUsernameIsExist("player"));
    }

    @Test
    public void testWhenCreatingRoomButTheRoomIdAlreadyExist() {
        Room roomExist = new Room("exist");
        when(roomRepository.findByRoomId("exist")).thenReturn(roomExist);
        assertEquals(null, gameService.createRoom("exist"));
    }

    @Test
    public void testWhenDeletingRoomThatExist() {
        Room deleteRoom = new Room("delete");
        deleteRoom.addPlayer(new Player("player1"));
        deleteRoom.addPlayer(new Player("player2"));
        deleteRoom.addPlayer(new Player("player3"));
        deleteRoom.addPlayer(new Player("player4"));
        when(roomRepository.findByRoomId("delete")).thenReturn(deleteRoom);
        assertEquals(true, gameService.deleteRoom("delete"));
    }

    @Test
    public void testWhenPlayerChoosingAnswer() {
        AnswerStat answerStat = new AnswerStat();
        when(statisticService.getAnswerStat("choose")).thenReturn(answerStat);
        Room room = new Room("choose");
        room.setQuestionSetId(1);
        when(roomRepository.findByRoomId("choose")).thenReturn(room);
        Player player = new Player("player");
        room.addPlayer(player);
        when(userRepository.findByUsername("player")).thenReturn(player);
        Scoreboard scoreboard = new Scoreboard();
        when(scoreboardService.getScoreBoard("choose")).thenReturn(scoreboard);
        QuestionSet questionSet = new QuestionSet();
        List<Question> questionList = new ArrayList<>();
        questionList.add(new Question());
        questionList.add(new Question());
        questionSet.setListOfQuestions(questionList);
        when(questionService.findQuestionSet(1)).thenReturn(questionSet);
        gameService.playerChooseAnswer("player", "A", "choose");
        assertEquals(true, true);
    }

    @Test
    public void testWhenPlayerChoosingAnswerWithNoQuestionSet() {
        AnswerStat answerStat = new AnswerStat();
        when(statisticService.getAnswerStat("choose")).thenReturn(answerStat);
        Room room = new Room("choose");
        room.setQuestionSetId(1);
        when(roomRepository.findByRoomId("choose")).thenReturn(room);
        Player player = new Player("player");
        room.addPlayer(player);
        when(userRepository.findByUsername("player")).thenReturn(player);
        Scoreboard scoreboard = new Scoreboard();
        when(scoreboardService.getScoreBoard("choose")).thenReturn(scoreboard);
        QuestionSet questionSet = new QuestionSet();
        List<Question> questionList = new ArrayList<>();
        questionList.add(new Question());
        questionList.add(new Question());
        questionSet.setListOfQuestions(questionList);
        when(questionService.findQuestionSet(1)).thenReturn(null);
        gameService.playerChooseAnswer("player", "A", "choose");
        assertEquals(true, true);
    }

    @Test
    public void testWhenPlayerChoosingAnswerWithCorrectAnswer() {
        AnswerStat answerStat = new AnswerStat();
        when(statisticService.getAnswerStat("choose")).thenReturn(answerStat);
        Room room = new Room("choose");
        room.setQuestionSetId(1);
        when(roomRepository.findByRoomId("choose")).thenReturn(room);
        Player player = new Player("player");
        room.addPlayer(player);
        when(userRepository.findByUsername("player")).thenReturn(player);
        Scoreboard scoreboard = new Scoreboard();
        when(scoreboardService.getScoreBoard("choose")).thenReturn(scoreboard);
        QuestionSet questionSet = new QuestionSet();
        List<Question> questionList = new ArrayList<>();
        Question question = new Question();
        question.setCorrectAnswer("A");
        questionList.add(question);
        questionSet.setListOfQuestions(questionList);
        when(questionService.findQuestionSet(1)).thenReturn(questionSet);
        gameService.playerChooseAnswer("player", "A", "choose");
        assertEquals(true, true);
    }

    @Test
    public void testWhenRoomIsWantToMoveToNextRoundWithQuestionSetExist() {
        Room room = new Room("nextRoundRoom");
        room.setQuestionSetId(1);
        when(roomRepository.findByRoomId("nextRoundRoom")).thenReturn(room);
        Question question = new Question();
        List<Question> questionList = new ArrayList<>();
        QuestionSet questionSet = new QuestionSet();
        question.setCorrectAnswer("A");
        questionList.add(question);
        questionSet.setListOfQuestions(questionList);
        when(questionService.findQuestionSet(1)).thenReturn(questionSet);
        assertEquals("GAMEOVER", gameService.nextRound("nextRoundRoom"));
    }

    @Test
    public void testWhenRoomIsWantToMoveToNextRoundButAlreadyFinished() {
        Room room = new Room("nextRoundRoom");
        room.setQuestionSetId(2);
        when(roomRepository.findByRoomId("nextRoundRoom")).thenReturn(room);
        Question question = new Question();
        question.setCorrectAnswer("A");
        List<Question> questionList = new ArrayList<>();
        questionList.add(question);
        questionList.add(new Question());
        QuestionSet questionSet = new QuestionSet();
        questionSet.setListOfQuestions(questionList);
        when(questionService.findQuestionSet(2)).thenReturn(questionSet);
        assertEquals("NEXT_ROUND", gameService.nextRound("nextRoundRoom"));
    }

    @Test
    public void testWhenRoomIsWantToMoveToNextRoundWithNoQuestionSet() {
        Room room = new Room("nextRoundRoom");
        room.setQuestionSetId(2);
        when(roomRepository.findByRoomId("nextRoundRoom")).thenReturn(room);
        Question question = new Question();
        question.setCorrectAnswer("A");
        List<Question> questionList = new ArrayList<>();
        questionList.add(question);
        questionList.add(new Question());
        QuestionSet questionSet = new QuestionSet();
        questionSet.setListOfQuestions(questionList);
        when(questionService.findQuestionSet(2)).thenReturn(null);
        assertEquals("GAMEOVER", gameService.nextRound("nextRoundRoom"));
    }

    @Test
    public void testWhenFindRoomIsCalledItShouldCallRoomRepositoryFindRoomById() {
        gameService.findRoom("roomId");
        verify(roomRepository, times(1)).findByRoomId("roomId");
    }

    @Test
    public void testCreateRoomIsCalledItShouldCheckIfRoomExistByCallingRoomRepositoriFindById() {
        gameService.createRoom("roomId");
        verify(roomRepository, times(1)).findByRoomId("roomId");
    }

    @Test
    public void testCreateRoomWhenRoomNotExist() {
        gameService.createRoom("roomId");
        verify(roomRepository, times(1)).registerRoom(any(Room.class));
        verify(statisticService, times(1)).addAnswerStat("roomId");
        verify(scoreboardService, times(1)).addScoreBoard("roomId");
    }

    @Test
    public void testDeleteRoomWhenRoomNotExist() {
        assertFalse(gameService.deleteRoom("roomId"));
        verify(roomRepository, times(1)).findByRoomId("roomId");
    }
}
