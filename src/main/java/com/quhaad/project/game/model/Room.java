package com.quhaad.project.game.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Room {

    private String roomId;

    private List<Player> players;

    private int round;

    private int questionSetId;

    public Room(String roomId) {
        this.roomId = roomId;
        this.players = new ArrayList<>();
        this.round = 0;
        this.questionSetId = -1;
    }

    public boolean checkIsTheRoomFull() {
        return players.size() >= 4;
    }

    public void addPlayer(Player player) {
        players.add(player);
    }

    public void setQuestionSetId(int questionSetId) {
        if (this.questionSetId == -1) {
            this.questionSetId = questionSetId;
        }
    }

    public int getQuestionSetId() {
        return questionSetId;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public boolean checkIsPlayerInThisRoom(Player player) {
        return players.contains(player);
    }

    public String getRoomId() {
        return roomId;
    }

    public int getRound() {
        return round;
    }

    public void nextRound() {
        this.round++;
        for (Player player : players) {
            player.resetAnswer();
        }
    }
}
