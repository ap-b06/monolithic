package com.quhaad.project.game.controller;

import com.quhaad.project.game.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class GameController {

    @Autowired
    private GameService gameService;

    /**
     * Form to create game room.
     * @param roomId String - room id to be created
     * @return String - create process status
     */
    @CrossOrigin(origins = "*")
    @PostMapping("/create/{roomId}")
    @ResponseBody
    public String createRoomId(@PathVariable String roomId) {
        boolean roomExist = gameService.findRoom(roomId);
        if (roomExist) {
            return "ERROR";
        } else {
            gameService.createRoom(roomId);
            return "NEXT";
        }
    }

    /**
     * Form to check game room.
     * @param roomId String - room id to be checked
     * @return String - status if game room exists or not
     */
    @CrossOrigin(origins = "*")
    @GetMapping("/check/{roomId}")
    @ResponseBody
    public String checkRoomId(@PathVariable String roomId) {
        boolean roomExist = gameService.findRoom(roomId);
        if (roomExist) {
            return "ROOM EXISTS";
        } else {
            return "ROOM DOES NOT EXIST";
        }
    }

    /**
     * Form to make next round.
     * @param roomId String - room id that its round want to be increased
     * @return void
     */
    @CrossOrigin(origins = "*")
    @GetMapping("/next-round/{roomId}")
    @ResponseBody
    public String nextRound(@PathVariable String roomId) {
        return gameService.nextRound(roomId);
    }

    /**
     * Delete room id.
     * @param roomId String - room id that want to be removed
     */
    @CrossOrigin(origins = "*")
    @DeleteMapping("/delete/{roomId}")
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteRoomId(@PathVariable String roomId) {
        gameService.deleteRoom(roomId);
    }
}
