package com.quhaad.project.question.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.quhaad.project.game.model.Room;
import com.quhaad.project.game.repository.RoomRepository;
import com.quhaad.project.question.core.Question;
import com.quhaad.project.question.core.QuestionSet;
import com.quhaad.project.question.service.QuestionServiceImplementation;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

@WebMvcTest(controllers = QuestionController.class)
public class QuestionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private QuestionServiceImplementation questionService;

    @MockBean
    private RoomRepository roomRepository;

    @Test
    public void whenQuestionUriIsAccessedShouldHandledByFindAll() throws Exception {
        mockMvc.perform(get("/question"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("findAll"));
    }

    @Test
    public void whenCreateUrlIsAccessedShouldAddAQuestionSet() throws Exception {
        Room room = new Room("room1");
        when(roomRepository.findByRoomId("room1")).thenReturn(room);
        QuestionSet questionSet = new QuestionSet();
        questionSet.setQuestionSetName("Mock Question");
        Question question = new Question();
        question.setQuestionText("This is a test.");
        question.setQuestionMediaUrl("testing.com");
        question.setAnswer1("A");
        question.setAnswer2("B");
        question.setAnswer3("C");
        question.setAnswer4("D");
        question.setCorrectAnswer("A");
        question.setTimeLimit(10);
        List<Question> questionList = new ArrayList<>();
        questionList.add(question);
        questionSet.setListOfQuestions(questionList);

        when(questionService.registerQuestionSet(any(QuestionSet.class))).thenReturn(questionSet);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(questionSet);

        MediaType jsonUtf8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        MockHttpServletRequestBuilder request = post("/question/{roomId}", "room1");
        request.content(requestJson);
        request.accept(jsonUtf8);
        request.contentType(jsonUtf8);

        mockMvc.perform(request
                .content(requestJson))
                .andExpect(status().isCreated());
    }

    @Test
    public void whenDeleteQuestionUriIsAccessedItShouldHandledByDelete() throws Exception {
        QuestionSet questionSet = new QuestionSet();
        int questionSetId = questionSet.getQuestionSetId();

        mockMvc.perform(delete("/question/{queryId}", questionSetId))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("delete"));
    }

    @Test
    public void whenQuestionUriAccessedByIdNotExistShouldThrowException() throws Exception {
        Room room1 = new Room("room1");
        room1.setQuestionSetId(11);
        when(roomRepository.findByRoomId("room1")).thenReturn(room1);
        try {
            mockMvc.perform(get("/question/{roomId}", "room1"));
        } catch (Exception e) {
            String result = "Request processing failed;";
            result += " nested exception is java.util.NoSuchElementException: ";
            result += "Question set not found!";
            assertEquals(result, e.getMessage());
        }
    }

    @Test
    public void whenQuestionUriIsAccessedToFindByIdThatExistItShouldOk() throws Exception {
        QuestionSet questionSet = new QuestionSet();
        questionSet.setQuestionSetName("Mock Question");

        Question question = new Question();
        question.setQuestionText("This is a test.");
        question.setQuestionMediaUrl("testing.com");
        question.setAnswer1("A");
        question.setAnswer2("B");
        question.setAnswer3("C");
        question.setAnswer4("D");
        question.setCorrectAnswer("A");
        question.setTimeLimit(10);

        List<Question> questionList = new ArrayList<>();
        questionList.add(question);
        questionSet.setListOfQuestions(questionList);

        when(questionService.findQuestionSet(11)).thenReturn(questionSet);
        Room room1 = new Room("room1");
        room1.setQuestionSetId(11);
        when(roomRepository.findByRoomId("room1")).thenReturn(room1);

        mockMvc.perform(get("/question/{roomId}", "room1"))
                .andExpect(handler().methodName("findById"))
                .andExpect(status().isOk());
    }
}