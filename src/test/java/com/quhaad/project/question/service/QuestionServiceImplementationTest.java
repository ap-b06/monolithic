package com.quhaad.project.question.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.quhaad.project.question.core.QuestionSet;
import com.quhaad.project.question.repository.QuestionSetRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class QuestionServiceImplementationTest {
    @Mock
    private QuestionSetRepository questionSetRepository;

    @InjectMocks
    private QuestionServiceImplementation questionService;

    @Test
    public void whenFindAllQuestionSetIsCalledItShouldCallQuestionSetRepositoryFindAll() {
        questionService.findAllQuestionSet();

        verify(questionSetRepository, times(1)).findAll();
    }

    @Test
    public void whenFindQuestionSetIsCalledItShouldCallQuestionSetRepositoryFindById() {
        questionService.findQuestionSet(1);

        verify(questionSetRepository, times(1)).findById(1);
    }

    @Test
    public void whenDeleteQuestionSetIsCalledItShouldCallQuestionSetRepositoryDeleteById() {
        questionService.deleteQuestionSet(1);

        verify(questionSetRepository, times(1)).deleteById(1);
    }

    @Test
    public void whenRegisterQuestionSetIsCalledItShouldCallQuestionSetRepositorySave() {
        QuestionSet questionSet = new QuestionSet();
        questionService.registerQuestionSet(questionSet);

        verify(questionSetRepository, times(1)).save(questionSet);
    }
}