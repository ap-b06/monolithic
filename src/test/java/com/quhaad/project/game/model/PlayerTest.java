package com.quhaad.project.game.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PlayerTest {
    private Player player;

    @BeforeEach
    public void setUp() {
        player = new Player("player");
    }

    @Test
    public void testEmptyConstructor() {
        player = new Player();
        assertTrue(true);
    }

    @Test
    public void testMethodGetAnswer() {
        assertEquals("Z", player.getAnswer());
    }

    @Test
    public void testMethodGetUsername() {
        assertEquals("player", player.getUsername());
    }

    @Test
    public void testMethodGetScore() {
        assertEquals(0, player.getScore());
    }

    @Test
    public void testMethodAddScore() {
        player.addScore(7);
        assertEquals(7, player.getScore());
    }

    @Test
    public void testMethodSetUsername() {
        player.setUsername("user");
        assertEquals("user", player.getUsername());
    }

    @Test
    public void testMethodSetAnswer() {
        player.setAnswer("A");
        assertEquals("A", player.getAnswer());
    }

    @Test
    public void testMethodSetScore() {
        player.setScore(1);
        assertEquals(1, player.getScore());
    }

    @Test
    public void testMethodChooseAnswer() {
        player.chooseAnswer("A");
        assertEquals("A", player.getAnswer());
        player.chooseAnswer("B");
        assertEquals("A", player.getAnswer());
    }

    @Test
    public void testMethodCorrectAnswer() {
        player.correctAnswer();
        assertEquals("CORRECT", player.getAnswer());
    }

    @Test
    public void testMethodWrongAnswer() {
        player.wrongAnswer();
        assertEquals("WRONG", player.getAnswer());
    }

    @Test
    public void testMethodResetAnswer() {
        player.resetAnswer();
        assertEquals("Z", player.getAnswer());
    }

    @Test
    public void testMethodCompareTo() {
        Player player1 = new Player("player1");
        player1.addScore(7);
        assertTrue(player.compareTo(player1) < 0);
    }
}
