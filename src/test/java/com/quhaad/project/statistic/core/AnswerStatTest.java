package com.quhaad.project.statistic.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AnswerStatTest {

    private AnswerStat answerStat;

    @BeforeEach
    public void setUp() {
        answerStat = new AnswerStat();
        answerStat.setAnswerACount(0);
        answerStat.setAnswerBCount(1);
        answerStat.setAnswerCCount(2);
        answerStat.setAnswerDCount(1);
    }

    @Test
    public void testComputeAnswers() {
        AnswerStat answerStatSpy = spy(answerStat);

        answerStatSpy.computeAnswer("A");
        verify(answerStatSpy, times(1)).incrementAnswerACount();

        answerStatSpy.computeAnswer("B");
        verify(answerStatSpy, times(1)).incrementAnswerBCount();

        answerStatSpy.computeAnswer("C");
        verify(answerStatSpy, times(1)).incrementAnswerCCount();

        answerStatSpy.computeAnswer("D");
        verify(answerStatSpy, times(1)).incrementAnswerDCount();

    }


    @Test
    public void testgetAnswerACount() {
        int answerACount = answerStat.getAnswerACount();
        assertEquals(0, answerACount);
    }

    @Test
    public void testgetAnswerBCount() {
        int answerBCount = answerStat.getAnswerBCount();
        assertEquals(1, answerBCount);
    }

    @Test
    public void testgetAnswerCCount() {
        int answerCCount = answerStat.getAnswerCCount();
        assertEquals(2, answerCCount);
    }

    @Test
    public void testgetAnswerDCount() {
        int answerDCount = answerStat.getAnswerDCount();
        assertEquals(1, answerDCount);
    }

    @Test
    public void testSetAnswerACount() {
        int newAnswer = 2;
        answerStat.setAnswerACount(newAnswer);
        int answerACount = answerStat.getAnswerACount();
        assertEquals(newAnswer, answerACount
        );
    }

    @Test
    public void testSetAnswerBCount() {
        int newAnswer = 2;
        answerStat.setAnswerBCount(newAnswer);
        int answerBCount = answerStat.getAnswerBCount();
        assertEquals(newAnswer, answerBCount
        );
    }

    @Test
    public void testSetAnswerCCount() {
        int newAnswer = 2;
        answerStat.setAnswerCCount(newAnswer);
        int answerCCount = answerStat.getAnswerCCount();
        assertEquals(newAnswer, answerCCount
        );
    }

    @Test
    public void testSetAnswerDCount() {
        int newAnswer = 2;
        answerStat.setAnswerDCount(newAnswer);
        int answerDCount = answerStat.getAnswerDCount();
        assertEquals(newAnswer, answerDCount
        );
    }

    @Test
    public void testIncrementAnswerACount() {
        answerStat.setAnswerACount(0);
        answerStat.incrementAnswerACount();
        int answerACount = answerStat.getAnswerACount();
        assertEquals(1, answerACount
        );
    }

    @Test
    public void testIncrementAnswerBCount() {
        answerStat.setAnswerBCount(0);
        answerStat.incrementAnswerBCount();
        int answerBCount = answerStat.getAnswerBCount();
        assertEquals(1, answerBCount
        );
    }

    @Test
    public void testIncrementAnswerCCount() {
        answerStat.setAnswerCCount(0);
        answerStat.incrementAnswerCCount();
        int answerCCount = answerStat.getAnswerCCount();
        assertEquals(1, answerCCount
        );
    }

    @Test
    public void testIncrementAnswerDCount() {
        answerStat.setAnswerDCount(0);
        answerStat.incrementAnswerDCount();
        int answerDCount = answerStat.getAnswerDCount();
        assertEquals(1, answerDCount
        );
    }

    @Test
    public void testRefreshAnswers() {
        answerStat.refreshAnswers();
        int answerACount = answerStat.getAnswerACount();
        assertEquals(0, answerACount);
        int answerBCount = answerStat.getAnswerBCount();
        assertEquals(0, answerBCount);
        int answerCCount = answerStat.getAnswerCCount();
        assertEquals(0, answerCCount);
        int answerDCount = answerStat.getAnswerDCount();
        assertEquals(0, answerDCount);
    }

}