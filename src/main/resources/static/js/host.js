'use strict';

var connectingElement = document.querySelector('.connecting');

var stompClient = Stomp.over(new SockJS('/ws'));
var roomId = null;

function getRoomId(event) {
    roomId = document.querySelector('.roomIdForm').value.trim();
}

function connect(event) {
    if (roomId) {
        stompClient.connect({}, onConnected, onError);
    }
    event.preventDefault();
}

function onConnected() {
    // Subscribe to the Public Topic
    stompClient.subscribe('/topic/room/' + roomId, onMessageReceived);

    var data = {}

    stompClient.send('/app/message/createRoom/' + roomId, {}, JSON.stringify(data));
}

function onError(error) {
    connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
    connectingElement.style.color = 'red';
}

function onMessageReceived(payload) {

    var message = JSON.parse(payload.body);

    if (message.content === 'CREATED') {
        // ENTER WAITING FOR PLAYER
    }
    else if (message.content === 'NEXT') {
        // NEXT ROUND
    }
}

function nextRound() {
    var data = {
        content: 'NEXT'
    };
    stompClient.send('/app/message/nextRound/' + roomId, {}, JSON.stringify(data));
}