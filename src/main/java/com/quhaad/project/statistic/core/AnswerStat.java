package com.quhaad.project.statistic.core;

public class AnswerStat {

    private int answerACount;

    private int answerBCount;

    private int answerCCount;

    private int answerDCount;

    public AnswerStat() {
        this.answerACount = 0;
        this.answerBCount = 0;
        this.answerCCount = 0;
        this.answerDCount = 0;
    }

    public void refreshAnswers() {
        setAnswerACount(0);
        setAnswerBCount(0);
        setAnswerCCount(0);
        setAnswerDCount(0);
    }

    /**
     * Count answers from all players.
     * @param answer String - answer
     */
    public void computeAnswer(String answer) {
        switch (answer) {
            case "A":
                incrementAnswerACount();
                break;
            case "B":
                incrementAnswerBCount();
                break;
            case "C":
                incrementAnswerCCount();
                break;
            case "D":
                incrementAnswerDCount();
                break;
            default:
                break;
        }
    }

    public void setAnswerACount(int answerACount) {
        this.answerACount = answerACount;
    }

    public void setAnswerBCount(int answerBcount) {
        this.answerBCount = answerBcount;
    }

    public void setAnswerCCount(int answerCCount) {
        this.answerCCount = answerCCount;
    }

    public void setAnswerDCount(int answerDCount) {
        this.answerDCount = answerDCount;
    }

    public void incrementAnswerACount() {
        this.answerACount++;
    }

    public void incrementAnswerBCount() {
        this.answerBCount++;
    }

    public void incrementAnswerCCount() {
        this.answerCCount++;
    }

    public void incrementAnswerDCount() {
        this.answerDCount++;
    }

    public int getAnswerACount() {
        return answerACount;
    }

    public int getAnswerBCount() {
        return answerBCount;
    }

    public int getAnswerCCount() {
        return answerCCount;
    }

    public int getAnswerDCount() {
        return answerDCount;
    }

}
