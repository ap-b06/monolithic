package com.quhaad.project.game.repository;

import com.quhaad.project.game.model.Room;
import java.util.HashMap;
import org.springframework.stereotype.Repository;

@Repository
public class RoomRepository {

    private HashMap<String, Room> roomMap = new HashMap<>();

    public void registerRoom(Room room) {
        getRoomMap().put(room.getRoomId(), room);
    }

    public Room findByRoomId(String roomId) {
        return getRoomMap().get(roomId);
    }

    public void deleteRoomById(String roomId) {
        getRoomMap().remove(roomId);
    }

    public HashMap<String, Room> getRoomMap() {
        return roomMap;
    }
}
