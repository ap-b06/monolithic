'use strict';

var connectingElement = document.querySelector('.connecting');

var stompClient = Stomp.over(new SockJS('/ws'));
var roomId = null;
var username = null;

var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];

function connect(event) {
    // CONNECT KE ROOM DAHULU
    roomId = document.querySelector('#roomId').value.trim();
    if (roomId) {
        stompClient.connect({}, onConnected, onError);
    }
    event.preventDefault();
}

function onConnected() {
    // Subscribe to the Public Topic
    stompClient.subscribe('/topic/room/' + roomId, onMessageReceived);

    // Tell your username to the server
    stompClient.send("/app/message/findRoom/" + roomId, {}, JSON.stringify({type: 'JOIN'}));
}

function onError(error) {
    connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
    connectingElement.style.color = 'red';
}

function onMessageReceived(payload) {

    var message = JSON.parse(payload.body);

    if (message.type === 'CONNECTED') {
        // CREATE USERNAME FORM
    } else if (message.type === 'FAILED') {
        document.querySelector('#roomIdForm').reset();
        stompClient.unsubscribe();
        stompClient.disconnect();
    } else if (message.type === 'SUCCEED' && username === message.receiver) {
        // LOADING SCREEN AND WAITING FOR THE GAME TO START
    } else if (message.type === 'RETRY' && username === message.receiver) {
        // BACK TO ENTER ROOM ID
    } else if (message.type === 'POSTED') {
        // WAITING FOR STATISTIC AND CORRECT ANSWER
    } else if (message.type === 'FEEDBACK') {
        if (message.content === 'CORRECT') {
            // GREEN CHECK
        }
        else {
            // RED CHECK
        }
    }
    // NOT DONE YET
}

function sendUsername(event) {
    username = document.querySelector('#username').value.trim();
    if (username) {
        var message = {
            sender: username,
            type: 'JOIN'
        }
    }
    stompClient.send('/app/message/username/' + roomId, {}, JSON.stringify(message));
    document.querySelector('#usernameForm').reset();
    event.preventDefault();
}

function buttonOne() {
    var data = {
        sender: username,
        content: 'A',
        type: 'ANSWER'
    };
    stompClient.send('/app/message/answer/' + roomId, {}, JSON.stringify(data));
}

function buttonTwo() {
    var data = {
        sender: username,
        content: 'B',
        type: 'ANSWER'
    };
    stompClient.send('/app/message/answer/' + roomId, {}, JSON.stringify(data));
}

function buttonThree() {
    var data = {
        sender: username,
        content: 'C',
        type: 'ANSWER'
    };
    stompClient.send('/app/message/answer/' + roomId, {}, JSON.stringify(data));
}

function buttonFour() {
    var data = {
        sender: username,
        content: 'D',
        type: 'ANSWER'
    };
    stompClient.send('/app/message/answer/' + roomId, {}, JSON.stringify(data));
}
