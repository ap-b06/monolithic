package com.quhaad.project.scoreboard.repository;

import com.quhaad.project.scoreboard.core.Scoreboard;
import java.util.HashMap;
import org.springframework.stereotype.Repository;

@Repository
public class ScoreboardRepository {

    private HashMap<String, Scoreboard> scoreboardMap = new HashMap<>();

    public void addScoreboardByRoomId(String roomId) {
        getScoreboardMap().put(roomId, new Scoreboard());
    }

    public Scoreboard getScoreboardByRoomId(String roomId) {
        return getScoreboardMap().get(roomId);
    }

    public void deleteScoreboardByRoomId(String roomId) {
        getScoreboardMap().remove(roomId);
    }

    public HashMap<String, Scoreboard> getScoreboardMap() {
        return scoreboardMap;
    }
}
