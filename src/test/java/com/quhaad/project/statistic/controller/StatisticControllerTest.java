package com.quhaad.project.statistic.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;

import com.quhaad.project.statistic.service.StatisticServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = StatisticController.class)
public class StatisticControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StatisticServiceImpl statisticService;

    @Test
    public void whenGetAnswerStatCalledShouldCallCorrectMethod() throws Exception {
        mockMvc.perform(get("/statistic/room1"))
                .andExpect(handler().methodName("getAnswerStat"));
    }
}
