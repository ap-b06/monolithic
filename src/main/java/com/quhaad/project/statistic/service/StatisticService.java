package com.quhaad.project.statistic.service;

import com.quhaad.project.statistic.core.AnswerStat;

public interface StatisticService {

    AnswerStat getAnswerStat(String roomId);

    void addAnswerStat(String roomId);

    void deleteAnswerStat(String roomId);

    void resetAnswerStat(String roomId);
}
