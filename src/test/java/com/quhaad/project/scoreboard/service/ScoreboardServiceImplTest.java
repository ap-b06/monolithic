package com.quhaad.project.scoreboard.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.quhaad.project.scoreboard.repository.ScoreboardRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ScoreboardServiceImplTest {

    @InjectMocks
    private ScoreboardServiceImpl scoreboardService;

    @Test
    public void testMethodGetScoreboardRepository() {
        ScoreboardRepository scoreboardRepository = new ScoreboardRepository();
        ScoreboardServiceImpl scoreboardServiceSpy = spy(scoreboardService);

        when(scoreboardServiceSpy.getScoreboardRepository()).thenReturn(scoreboardRepository);
        ScoreboardRepository calledRepository = scoreboardServiceSpy.getScoreboardRepository();
        assertEquals(scoreboardRepository, calledRepository);
    }

    @Test
    public void testMethodGetScoreboard() {
        ScoreboardRepository scoreboardRepositorySpy = spy(new ScoreboardRepository());
        ScoreboardServiceImpl scoreboardServiceSpy = spy(scoreboardService);

        when(scoreboardServiceSpy.getScoreboardRepository()).thenReturn(scoreboardRepositorySpy);
        scoreboardServiceSpy.getScoreBoard("roomId");
        verify(scoreboardRepositorySpy, times(1)).getScoreboardByRoomId("roomId");
    }

    @Test
    public void testMethodAddScoreboard() {
        ScoreboardRepository scoreboardRepositorySpy = spy(new ScoreboardRepository());
        ScoreboardServiceImpl scoreboardServiceSpy = spy(scoreboardService);

        when(scoreboardServiceSpy.getScoreboardRepository()).thenReturn(scoreboardRepositorySpy);
        scoreboardServiceSpy.addScoreBoard("roomId");
        verify(scoreboardRepositorySpy, times(1)).addScoreboardByRoomId("roomId");
    }

    @Test
    public void testMethodDeleteScoreboard() {
        ScoreboardRepository scoreboardRepositorySpy = spy(new ScoreboardRepository());
        ScoreboardServiceImpl scoreboardServiceSpy = spy(scoreboardService);

        when(scoreboardServiceSpy.getScoreboardRepository()).thenReturn(scoreboardRepositorySpy);
        scoreboardServiceSpy.deleteScoreBoard("roomId");
        verify(scoreboardRepositorySpy, times(1)).deleteScoreboardByRoomId("roomId");
    }
}
