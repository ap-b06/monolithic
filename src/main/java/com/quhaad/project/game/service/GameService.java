package com.quhaad.project.game.service;

import com.quhaad.project.game.model.Player;
import com.quhaad.project.game.model.Room;
import org.springframework.stereotype.Service;

public interface GameService {
    public String enterGame(String username, String roomId);

    public Room createRoom(String roomId);

    public boolean findRoom(String roomId);

    public boolean deleteRoom(String roomId);

    public void playerChooseAnswer(String username, String answer, String roomId);

    public boolean checkIfUsernameIsExist(String username);

    public String nextRound(String roomId);
}
