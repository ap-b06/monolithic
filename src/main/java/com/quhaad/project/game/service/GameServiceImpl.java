package com.quhaad.project.game.service;

import com.quhaad.project.game.model.Player;
import com.quhaad.project.game.model.Room;
import com.quhaad.project.game.repository.RoomRepository;
import com.quhaad.project.game.repository.UserRepository;
import com.quhaad.project.question.core.Question;
import com.quhaad.project.question.core.QuestionSet;
import com.quhaad.project.question.service.QuestionService;
import com.quhaad.project.scoreboard.core.Scoreboard;
import com.quhaad.project.scoreboard.service.ScoreboardService;
import com.quhaad.project.statistic.core.AnswerStat;
import com.quhaad.project.statistic.service.StatisticService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameServiceImpl implements GameService {

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private StatisticService statisticService;

    @Autowired
    private ScoreboardService scoreboardService;

    @Override
    public String enterGame(String username, String roomId) {
        boolean roomIsFull = checkIsTheRoomFull(roomId);
        if (roomIsFull) {
            return "ROOM_FULL";
        }
        addPlayerInfoToDatabase(username, roomId);
        return "SUCCESS";
    }

    private void addPlayerInfoToDatabase(String username, String roomId) {
        Player newPlayer = new Player(username);
        userRepository.registerPlayer(newPlayer);
        Scoreboard scoreboard = scoreboardService.getScoreBoard(roomId);
        scoreboard.addPlayer(newPlayer);
        Room gameRoom = roomRepository.findByRoomId(roomId);
        gameRoom.addPlayer(newPlayer);
    }

    private boolean checkIsTheRoomFull(String roomId) {
        Room gameRoom = roomRepository.findByRoomId(roomId);
        return gameRoom.checkIsTheRoomFull();
    }

    @Override
    public boolean checkIfUsernameIsExist(String username) {
        return userRepository.checkIfUsernameExist(username);
    }

    @Override
    public boolean findRoom(String roomId) {
        Room gameRoom = roomRepository.findByRoomId(roomId);
        return gameRoom != null;
    }

    @Override
    public Room createRoom(String roomId) {
        boolean roomIsExist = roomRepository.findByRoomId(roomId) != null;
        if (roomIsExist) {
            return null;
        } else {
            Room room = new Room(roomId);
            roomRepository.registerRoom(room);
            statisticService.addAnswerStat(roomId);
            scoreboardService.addScoreBoard(roomId);
            return room;
        }
    }

    @Override
    public boolean deleteRoom(String roomId) {
        boolean roomIsExist = roomRepository.findByRoomId(roomId) != null;
        if (roomIsExist) {
            deletePlayerInfoInSpecificRoom(roomId);
            roomRepository.deleteRoomById(roomId);
            statisticService.deleteAnswerStat(roomId);
            scoreboardService.deleteScoreBoard(roomId);
            return true;
        } else {
            return false;
        }
    }

    private void deletePlayerInfoInSpecificRoom(String roomId) {
        Room room = roomRepository.findByRoomId(roomId);
        List<Player> listOfPlayers = room.getPlayers();
        for (Player player : listOfPlayers) {
            userRepository.deleteByUsername(player.getUsername());
        }
    }

    private boolean checkIsPlayerInThisRoom(Player player, Room room) {
        return room.checkIsPlayerInThisRoom(player);
    }

    @Override
    public void playerChooseAnswer(String username, String answer, String roomId) {
        AnswerStat answerStat = statisticService.getAnswerStat(roomId);
        Room room = roomRepository.findByRoomId(roomId);
        Player player = userRepository.findByUsername(username);
        if (checkIsPlayerInThisRoom(player, room)) {
            answerStat.computeAnswer(answer);
            player.chooseAnswer(answer);
            checkPlayersAnswerInRoom(player, roomId);
        }
    }

    private Question getCurrentQuestion(Room room) {
        int questionSetIdInRoom = room.getQuestionSetId();
        int currentRoundInRoom = room.getRound();
        QuestionSet questionSet = questionService.findQuestionSet(questionSetIdInRoom);
        if (questionSet != null) {
            List<Question> questionList = questionSet.getListOfQuestions();
            return questionList.get(currentRoundInRoom);
        }
        return null;
    }

    private void checkPlayersAnswerInRoom(Player player, String roomId) {
        Scoreboard scoreboard = scoreboardService.getScoreBoard(roomId);
        Room room = roomRepository.findByRoomId(roomId);
        Question currentQuestion = getCurrentQuestion(room);
        if (currentQuestion != null) {
            if (currentQuestion.checkAnswer(player.getAnswer())) {
                player.correctAnswer();
                addPlayerScore(player, currentQuestion);
                scoreboard.updatePlayer(player);
            } else {
                player.wrongAnswer();
            }
        }
    }

    private void addPlayerScore(Player player, Question question) {
        player.addScore(question.getPoint());
        question.decrementPoint();
    }

    @Override
    public String nextRound(String roomId) {
        statisticService.resetAnswerStat(roomId);
        Room room = roomRepository.findByRoomId(roomId);
        room.nextRound();
        boolean gameOver = isRoundOver(roomId);
        if (gameOver) {
            return "GAMEOVER";
        }
        return "NEXT_ROUND";
    }

    private boolean isRoundOver(String roomId) {
        Room room = roomRepository.findByRoomId(roomId);
        int roomQuestionSetId = room.getQuestionSetId();
        QuestionSet roomQuestionSet = questionService.findQuestionSet(roomQuestionSetId);
        if (roomQuestionSet != null) {
            List<Question> listOfQuestions = roomQuestionSet.getListOfQuestions();
            int overRound = listOfQuestions.size();
            return overRound == room.getRound();
        }
        return true;
    }
}
