package com.quhaad.project.statistic.controller;

import com.quhaad.project.statistic.core.AnswerStat;
import com.quhaad.project.statistic.service.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatisticController {

    @Autowired
    private StatisticService statisticService;

    @GetMapping("/statistic/{roomId}")
    public ResponseEntity<AnswerStat> getAnswerStat(@PathVariable String roomId) {
        AnswerStat answerStat = statisticService.getAnswerStat(roomId);
        return new ResponseEntity<>(answerStat, HttpStatus.OK);
    }
}