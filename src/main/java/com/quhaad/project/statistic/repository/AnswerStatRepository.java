package com.quhaad.project.statistic.repository;

import com.quhaad.project.statistic.core.AnswerStat;
import java.util.HashMap;
import org.springframework.stereotype.Repository;

@Repository
public class AnswerStatRepository {

    private HashMap<String, AnswerStat> answerStatMap = new HashMap<>();

    public void addAnswerStatByRoomId(String roomId) {
        getAnswerStatMap().put(roomId, new AnswerStat());
    }

    public AnswerStat getAnswerStatByRoomId(String roomId) {
        return getAnswerStatMap().get(roomId);
    }

    public void deleteAnswerStatByRoomId(String roomId) {
        getAnswerStatMap().remove(roomId);
    }

    public void resetAnswerStatByRoomId(String roomId) {
        getAnswerStatMap().get(roomId).refreshAnswers();
    }

    public HashMap<String, AnswerStat> getAnswerStatMap() {
        return answerStatMap;
    }
}
