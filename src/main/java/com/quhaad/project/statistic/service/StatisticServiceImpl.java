package com.quhaad.project.statistic.service;

import com.quhaad.project.statistic.core.AnswerStat;
import com.quhaad.project.statistic.repository.AnswerStatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatisticServiceImpl implements StatisticService {

    @Autowired
    private AnswerStatRepository answerStatRepository;

    @Override
    public AnswerStat getAnswerStat(String roomId) {
        return answerStatRepository.getAnswerStatByRoomId(roomId);
    }

    @Override
    public void addAnswerStat(String roomId) {
        answerStatRepository.addAnswerStatByRoomId(roomId);
    }

    @Override
    public void deleteAnswerStat(String roomId) {
        answerStatRepository.deleteAnswerStatByRoomId(roomId);
    }

    @Override
    public void resetAnswerStat(String roomId) {
        answerStatRepository.resetAnswerStatByRoomId(roomId);
    }
}
