package com.quhaad.project.game.controller;

import com.quhaad.project.game.model.ChatMessage;
import com.quhaad.project.game.model.Player;
import com.quhaad.project.game.repository.RoomRepository;
import com.quhaad.project.game.service.GameService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;


@Controller
public class ChatController {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private GameService gameService;

    @Autowired
    private RoomRepository roomRepository;

    /**
     * Send submitted answer.
     * @param chatMessage answer
     * @param roomId room id
     */
    @MessageMapping("/message/answer/{roomId}")
    public void sendAnswer(@Payload ChatMessage chatMessage,
                           @DestinationVariable String roomId
    ) {
        if (chatMessage.getType() == ChatMessage.MessageType.ANSWER) {
            ChatMessage reply = new ChatMessage();
            String sender = chatMessage.getSender();
            gameService.playerChooseAnswer(sender, chatMessage.getContent(), roomId);
            reply.setType(ChatMessage.MessageType.POSTED);
            reply.setReceiver(chatMessage.getSender());
            simpMessagingTemplate.convertAndSend("/topic/room/" + roomId, reply);
        }
    }

    /**
     * Send username of the player.
     * @param chatMessage chatMessage object from player
     * @param roomId room id
     */
    @MessageMapping("/message/username/{roomId}")
    public void sendUsername(@Payload ChatMessage chatMessage,
                             @DestinationVariable String roomId
    ) {
        ChatMessage reply = new ChatMessage();
        boolean usernameIsExist = gameService.checkIfUsernameIsExist(chatMessage.getSender());
        if (!usernameIsExist) {
            String status = gameService.enterGame(chatMessage.getSender(), roomId);
            reply = replyGeneratorToJoinRoom(reply, status, chatMessage.getSender());
        } else {
            reply.setReceiver(chatMessage.getSender());
            reply.setContent("REINSERT");
            reply.setType(ChatMessage.MessageType.RETRY);
        }
        simpMessagingTemplate.convertAndSend("/topic/room/" + roomId, reply);
    }

    private ChatMessage replyGeneratorToJoinRoom(ChatMessage reply, String status, String sender) {
        if (status.equals("SUCCESS")) {
            reply.setType(ChatMessage.MessageType.SUCCEED);
            reply.setReceiver(sender);
        } else {
            reply.setReceiver(sender);
            reply.setContent("ROOM_FULL");
            reply.setType(ChatMessage.MessageType.RETRY);
        }
        return reply;
    }

    /**
     * Find room by Room Id.
     * @param chatMessage chatMessage
     * @param roomId room id
     */
    @MessageMapping("/message/findRoom/{roomId}")
    public void findRoom(@Payload ChatMessage chatMessage,
                         @DestinationVariable String roomId
    ) {
        ChatMessage reply = new ChatMessage();
        if (gameService.findRoom(roomId) && chatMessage.getType() == ChatMessage.MessageType.JOIN) {
            reply.setType(ChatMessage.MessageType.CONNECTED);
        } else {
            reply.setType(ChatMessage.MessageType.FAILED);
        }
        simpMessagingTemplate.convertAndSend("/topic/room/" + roomId, reply);
    }

    /**
     * Create game room.
     * @param roomId String - room id
     */
    @MessageMapping("/message/createRoom/{roomId}")
    public void createRoom(@DestinationVariable String roomId) {
        ChatMessage reply = new ChatMessage();
        reply.setContent("CREATED");
        gameService.createRoom(roomId);
        simpMessagingTemplate.convertAndSend("/topic/room/" + roomId, reply);
    }

    /**
     * Set state to nextRound.
     * @param roomId String - room id
     */
    @MessageMapping("/message/nextRound/{roomId}")
    public void nextRound(@DestinationVariable String roomId) {
        ChatMessage reply = new ChatMessage();
        reply.setContent("NEXT");
        gameService.nextRound(roomId);
        simpMessagingTemplate.convertAndSend("/topic/room/" + roomId, reply);
    }

    /**
     *  Send feedback from player's answer.
     * @param roomId Stirng - roomId
     */
    @MessageMapping("/message/host/{roomId}")
    public void sendFeedback(@DestinationVariable String roomId) {
        ChatMessage reply = new ChatMessage();
        reply.setType(ChatMessage.MessageType.FEEDBACK);
        List<Player> playerList = roomRepository.findByRoomId(roomId).getPlayers();
        for (Player player : playerList) {
            reply.setReceiver(player.getUsername());
            reply.setContent(player.getAnswer());
            simpMessagingTemplate.convertAndSend("/topic/room/" + roomId, reply);
        }
    }

    /**
     * Send message during playgame.
     * @param chatMessage chatMessage
     * @param roomId room id
     */
    @MessageMapping("/message/all/{roomId}")
    public void sendMessage(@Payload ChatMessage chatMessage, @DestinationVariable String roomId) {
        ChatMessage reply = new ChatMessage();
        reply.setType(ChatMessage.MessageType.PLAY);
        if (chatMessage.getContent().equals("ANSWER")) {
            reply.setContent("ANSWERED");
        }
        List<Player> playerList = roomRepository.findByRoomId(roomId).getPlayers();
        for (Player player : playerList) {
            reply.setReceiver(player.getUsername() + "");
            simpMessagingTemplate.convertAndSend("/topic/room/" + roomId, reply);
        }
    }

    /**
     * Show question to all players.
     * @param roomId String - room id
     */
    @MessageMapping("/message/show-question/{roomId}")
    public void sendQuestion(@DestinationVariable String roomId) {
        ChatMessage reply = new ChatMessage();
        reply.setType(ChatMessage.MessageType.SHOW);
        List<Player> playerList = roomRepository.findByRoomId(roomId).getPlayers();
        for (Player player : playerList) {
            reply.setReceiver(player.getUsername() + "");
            simpMessagingTemplate.convertAndSend("/topic/room/" + roomId, reply);
        }
    }
}