package com.quhaad.project.scoreboard.service;

import com.quhaad.project.scoreboard.core.Scoreboard;

public interface ScoreboardService {

    Scoreboard getScoreBoard(String roomId);

    void addScoreBoard(String roomId);

    void deleteScoreBoard(String roomId);

}
