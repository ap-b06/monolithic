package com.quhaad.project.question.controller;

import com.quhaad.project.game.model.Room;
import com.quhaad.project.game.repository.RoomRepository;
import com.quhaad.project.question.core.QuestionSet;
import com.quhaad.project.question.service.QuestionService;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/question")
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @Autowired
    private RoomRepository roomRepository;

    /**
     * Find all question sets.
     * @return Response Entity containing all question sets
     */
    @CrossOrigin(origins = "*")
    @GetMapping
    public ResponseEntity<List<QuestionSet>> findAll() {
        List<QuestionSet> questionSets = questionService.findAllQuestionSet();
        return new ResponseEntity<>(questionSets, HttpStatus.OK);
    }

    /**
     * Create question set.
     * @param questionSet QuestionSet to be created.
     * @param roomId String - room id
     * @return Response Entity containing created Question Set
     */
    @CrossOrigin(origins = "*")
    @PostMapping(consumes = {
            MediaType.APPLICATION_JSON_VALUE
            }, produces = {
            MediaType.APPLICATION_JSON_VALUE
            }, path = "/{roomId}")
    public ResponseEntity<QuestionSet> create(@RequestBody QuestionSet questionSet,
                                              @PathVariable String roomId) {
        QuestionSet newQuestionSet = questionService
                .registerQuestionSet(questionSet);
        Room room = roomRepository.findByRoomId(roomId);
        room.setQuestionSetId(newQuestionSet.getQuestionSetId());
        return new ResponseEntity<>(newQuestionSet, HttpStatus.CREATED);
    }

    /**
     * Find question set by room id.
     * @param roomId String - room id
     * @return search process status
     */
    @CrossOrigin(origins = "*")
    @GetMapping("/{roomId}")
    public ResponseEntity<QuestionSet> findById(@PathVariable String roomId) {
        Room room = roomRepository.findByRoomId(roomId);
        int queryId = room.getQuestionSetId();
        System.out.println("query" + queryId);
        if (questionService.findQuestionSet(queryId) == null) {
            throw new NoSuchElementException("Question set not found!");
        } else {
            return new ResponseEntity<>(questionService.findQuestionSet(queryId),
                    HttpStatus.OK);
        }
    }

    /**
     * Delete question set by id.
     * @param queryId String - question set id to be deleted
     * @return delete process status
     */
    @CrossOrigin(origins = "*")
    @DeleteMapping("/{queryId}")
    public ResponseEntity delete(@PathVariable int queryId) {
        questionService.deleteQuestionSet(queryId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
