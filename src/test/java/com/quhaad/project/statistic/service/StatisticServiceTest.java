package com.quhaad.project.statistic.service;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.quhaad.project.statistic.repository.AnswerStatRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class StatisticServiceTest {

    @Mock
    private AnswerStatRepository answerStatRepository;

    @InjectMocks
    private StatisticServiceImpl statisticService;

    @Test
    public void testgetAnswerStatMethodShouldCallgetAnswerStatbyRoomIdMethod() {
        StatisticServiceImpl spiedStatisticService = spy(statisticService);
        spiedStatisticService.addAnswerStat("roomId");
        spiedStatisticService.getAnswerStat("roomId");
        verify(answerStatRepository, times(1)).getAnswerStatByRoomId("roomId");
    }

    @Test
    public void testaddAnswerStatMethodShouldCalladdAnswerStatbyRoomIdMethod() {
        StatisticServiceImpl spiedStatisticService = spy(statisticService);

        spiedStatisticService.addAnswerStat("roomId");
        verify(answerStatRepository, times(1)).addAnswerStatByRoomId("roomId");
    }

    @Test
    public void testdeleteAnswerStatMethodShouldCalldeleteAnswerStatbyRoomIdMethod() {
        StatisticServiceImpl spiedStatisticService = spy(statisticService);

        spiedStatisticService.addAnswerStat("roomId");
        spiedStatisticService.deleteAnswerStat("roomId");
        verify(answerStatRepository, times(1)).deleteAnswerStatByRoomId("roomId");
    }

    @Test
    public void resetAnswerStatMethodShouldCallresetAnswerStatbyRoomIdMethod() {
        StatisticServiceImpl spiedStatisticService = spy(statisticService);

        spiedStatisticService.addAnswerStat("roomId");
        spiedStatisticService.resetAnswerStat("roomId");
        verify(answerStatRepository, times(1)).resetAnswerStatByRoomId("roomId");
    }
}
