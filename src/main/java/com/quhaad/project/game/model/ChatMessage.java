package com.quhaad.project.game.model;

public class ChatMessage {

    private MessageType type;

    private String content;

    private String sender;

    private String receiver;

    public enum MessageType {
        CONNECTED, // BERHASIL MASUK ROOM TAPI BLM ADA USERNAME
        SUCCEED, // BERHASIL MASUK ROOM DAN UDAH ADA USERNAME
        FAILED, // SALAH MEMASUKKAN ROOMID
        RETRY, // USERNAME YANG DIBERIKAN SUDAH ADA DIDALAM ROOM TERSEBUT
        ANSWER, // PLAYER MENGIRIM JAWABAN
        POSTED, // TANDA BAHWA JAWABAN SUDAH DIINPUT
        FEEDBACK,
        JOIN,
        LEAVE,
        PLAY, // BERSIAP-SIAP SEBELUM MASUK
        SHOW // MENUNJUKKAN PERTANYAAN
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }
}
