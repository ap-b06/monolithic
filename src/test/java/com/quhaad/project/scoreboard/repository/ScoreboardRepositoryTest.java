package com.quhaad.project.scoreboard.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.quhaad.project.scoreboard.core.Scoreboard;
import java.util.HashMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

public class ScoreboardRepositoryTest {

    @InjectMocks
    private ScoreboardRepository scoreboardRepository;
    private ScoreboardRepository scoreboardRepositorySpy;

    @BeforeEach
    public void setUp() {
        scoreboardRepository = new ScoreboardRepository();
        scoreboardRepositorySpy = spy(scoreboardRepository);
    }

    @Test
    public void testMethodGetScoreboardMap() {
        HashMap<String, Scoreboard> scoreboardMap = new HashMap<>();
        when(scoreboardRepositorySpy.getScoreboardMap()).thenReturn(scoreboardMap);
        assertEquals(scoreboardMap, scoreboardRepositorySpy.getScoreboardMap());
    }

    @Test
    public void testMethodAddScoreboardByRoomId() {
        HashMap<String, Scoreboard> scoreboardMapSpy;
        scoreboardMapSpy = spy(scoreboardRepositorySpy.getScoreboardMap());
        when(scoreboardRepositorySpy.getScoreboardMap()).thenReturn(scoreboardMapSpy);
        scoreboardRepositorySpy.addScoreboardByRoomId("roomId");
        verify(scoreboardMapSpy, times(1)).put(eq("roomId"), any(Scoreboard.class));
        assertEquals(1, scoreboardMapSpy.size());
    }

    @Test
    public void testMethodGetScoreboardByRoomId() {
        HashMap<String, Scoreboard> scoreboardMap = new HashMap<>();
        Scoreboard scoreboard = new Scoreboard();
        scoreboardMap.put("roomId", scoreboard);
        HashMap<String, Scoreboard> scoreboardMapSpy = spy(scoreboardMap);

        when(scoreboardRepositorySpy.getScoreboardMap()).thenReturn(scoreboardMapSpy);
        when(scoreboardMapSpy.get("roomId")).thenReturn(scoreboard);

        Scoreboard calledScoreboard = scoreboardRepositorySpy.getScoreboardByRoomId("roomId");
        assertEquals(scoreboard, calledScoreboard);
        verify(scoreboardMapSpy, times(1)).get("roomId");
    }

    @Test
    public void testMethodDeleteScoreboardByRoomId() {
        HashMap<String, Scoreboard> scoreboardMap = new HashMap<>();
        Scoreboard scoreboard = new Scoreboard();
        scoreboardMap.put("roomId", scoreboard);
        HashMap<String, Scoreboard> scoreboardMapSpy = spy(scoreboardMap);

        when(scoreboardRepositorySpy.getScoreboardMap()).thenReturn(scoreboardMapSpy);

        assertEquals(1, scoreboardMapSpy.size());
        scoreboardRepositorySpy.deleteScoreboardByRoomId("roomId");
        assertTrue(scoreboardMapSpy.isEmpty());
        verify(scoreboardMapSpy, times(1)).remove("roomId");
    }
}
