package com.quhaad.project.game.model;

public class Player implements Comparable<Player> {

    private String username;

    private String answer;

    private int score;

    public Player() {

    }

    public Player(String username) {
        this.username = username;
        this.answer = "Z"; // initial answer
        this.score = 0;
    }

    public String getUsername() {
        return username;
    }

    public int getScore() {
        return score;
    }

    public void addScore(int score) {
        this.score += score;
    }

    public String getAnswer() {
        return answer;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void chooseAnswer(String answer) {
        if (getAnswer().equals("Z")) {
            this.answer = answer;
        }
    }

    public void correctAnswer() {
        this.answer = "CORRECT";
    }

    public void wrongAnswer() {
        this.answer = "WRONG";
    }

    public void resetAnswer() {
        this.answer = "Z";
    }

    @Override
    public int compareTo(Player o) {
        return this.getScore() - o.getScore();
    }
}
