package com.quhaad.project.question.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class QuestionTest {

    private Question question;

    @BeforeEach
    public void setUp() {
        question = new Question();
        question.setQuestionText("Siapa presiden Indonesia?");
        question.setQuestionMediaUrl(null);
        question.setAnswer1("Jokowi");
        question.setAnswer2("Jacinda");
        question.setAnswer3("SBY");
        question.setAnswer4("Boris");
        question.setCorrectAnswer("A");
        question.setTimeLimit(10);
        question.setPoint(10);
    }

    @Test
    public void testGetQuestionText() {
        String textQuestion = question.getQuestionText();
        assertEquals("Siapa presiden Indonesia?", textQuestion);
    }

    @Test
    public void testGetQuestionMediaUrl() {
        String questionMediaUrl = question.getQuestionMediaUrl();
        assertNull(questionMediaUrl);
    }

    @Test
    public void testGetAnswer1() {
        String answer1 = question.getAnswer1();
        assertEquals("Jokowi", answer1);
    }

    @Test
    public void testGetAnswer2() {
        String answer2 = question.getAnswer2();
        assertEquals("Jacinda", answer2);
    }

    @Test
    public void testGetAnswer3() {
        String answer3 = question.getAnswer3();
        assertEquals("SBY", answer3);
    }

    @Test
    public void testGetAnswer4() {
        String answer4 = question.getAnswer4();
        assertEquals("Boris", answer4);
    }

    @Test
    public void testGetCorrectAnswer() {
        String correctAnswer = question.getCorrectAnswer();
        assertEquals("A", correctAnswer);
    }

    @Test
    public void testGetTimeLimit() {
        int timeLimit = question.getTimeLimit();
        assertEquals(10, timeLimit);
    }

    @Test
    public void testSetQuestionText() {
        String newQuestionText = "Siapa presiden Indonesia ke-6?";
        question.setQuestionText(newQuestionText);
        String actualQuestionText = question.getQuestionText();
        assertEquals(newQuestionText, actualQuestionText);
    }

    @Test
    public void testSetQuestionMediaUrl() {
        String newQuestionMediaUrl = "https://unsplash.com/";
        question.setQuestionMediaUrl(newQuestionMediaUrl);
        String actualQuestionMediaUrl = question.getQuestionMediaUrl();
        assertEquals(newQuestionMediaUrl, actualQuestionMediaUrl);
    }

    @Test
    public void testSetAnswer1() {
        String newAnswer1 = "Jacinda";
        question.setAnswer1(newAnswer1);
        String actualAnswer1 = question.getAnswer1();
        assertEquals(newAnswer1, actualAnswer1);
    }

    @Test
    public void testSetAnswer2() {
        String newAnswer2 = "Justin";
        question.setAnswer2(newAnswer2);
        String actualAnswer2 = question.getAnswer2();
        assertEquals(newAnswer2, actualAnswer2);
    }

    @Test
    public void testSetAnswer3() {
        String newAnswer3 = "Jokowi";
        question.setAnswer3(newAnswer3);
        String actualAnswer3 = question.getAnswer3();
        assertEquals(newAnswer3, actualAnswer3);
    }

    @Test
    public void testSetAnswer4() {
        String newAnswer4 = "SBY";
        question.setAnswer4(newAnswer4);
        String actualAnswer4 = question.getAnswer4();
        assertEquals(newAnswer4, actualAnswer4);
    }

    @Test
    public void testSetCorrectAnswer() {
        String newCorrectAnswer = "4";
        question.setCorrectAnswer(newCorrectAnswer);
        String actualCorrectAnswer = question.getCorrectAnswer();
        assertEquals(newCorrectAnswer, actualCorrectAnswer);
    }

    @Test
    public void testSetTimeLimit() {
        int newTimeLimit = 20;
        question.setTimeLimit(newTimeLimit);
        int actualTimeLimit = question.getTimeLimit();
        assertEquals(newTimeLimit, actualTimeLimit);
    }

    @Test
    public void testSetPoint() {
        int point = 12;
        question.setPoint(point);
        int actualPoint = question.getPoint();
        assertEquals(point, actualPoint);
    }

    @Test
    public void testGetPoint() {
        int point = question.getPoint();
        assertEquals(10, point);
    }

    @Test
    public void testDecrementPoint() {
        int point = question.getPoint();
        String questionText = question.getQuestionText();
        int decrementedPoint = point - 1;
        question.decrementPoint();
        int actualDecrementedPoint = question.getPoint();
        assertEquals(decrementedPoint, actualDecrementedPoint);
    }

    @Test
    public void testCheckAnswer() {
        assertTrue(question.checkAnswer("A"));
        assertFalse(question.checkAnswer("B"));
        assertFalse(question.checkAnswer("C"));
        assertFalse(question.checkAnswer("D"));
    }
}