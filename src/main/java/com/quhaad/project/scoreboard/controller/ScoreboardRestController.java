package com.quhaad.project.scoreboard.controller;

import com.quhaad.project.scoreboard.core.Scoreboard;
import com.quhaad.project.scoreboard.service.ScoreboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@RequestMapping(path = "/scoreboard-api")
public class ScoreboardRestController {

    @Autowired
    private final ScoreboardService scoreboardService;

    public ScoreboardRestController(ScoreboardService scoreboardService) {
        this.scoreboardService = scoreboardService;
    }

    @GetMapping("/scoreboard/{roomId}")
    public ResponseEntity<Scoreboard> getScoreBoard(@PathVariable String roomId) {
        Scoreboard scoreboard = scoreboardService.getScoreBoard(roomId);
        return new ResponseEntity<>(scoreboard, HttpStatus.OK);
    }
}
