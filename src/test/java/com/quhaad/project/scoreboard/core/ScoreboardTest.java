package com.quhaad.project.scoreboard.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.quhaad.project.game.model.Player;
import java.util.PriorityQueue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

public class ScoreboardTest {

    @InjectMocks
    private Scoreboard scoreboard;

    @BeforeEach
    public void setUp() {
        scoreboard = new Scoreboard();
    }

    @Test
    public void testMethodGetPq() {
        PriorityQueue<Player> pq = new PriorityQueue<>();

        Scoreboard scoreboardSpy = spy(scoreboard);
        when(scoreboardSpy.getPq()).thenReturn(pq);

        PriorityQueue<Player> calledPq = scoreboardSpy.getPq();

        assertEquals(pq, calledPq);
    }

    @Test
    public void testMethodAddPlayer() {
        PriorityQueue<Player> pqSpy = spy(scoreboard.getPq());
        Scoreboard scoreboardSpy = spy(scoreboard);
        Player player = new Player("username");

        when(scoreboardSpy.getPq()).thenReturn(pqSpy);

        scoreboardSpy.addPlayer(player);
        verify(pqSpy, times(1)).add(player);
        assertEquals(1, pqSpy.size());
    }

    @Test
    public void testMethodUpdatePlayer() {
        PriorityQueue<Player> pqSpy = spy(scoreboard.getPq());
        Scoreboard scoreboardSpy = spy(scoreboard);
        Player player = new Player("username");

        when(scoreboardSpy.getPq()).thenReturn(pqSpy);

        scoreboardSpy.updatePlayer(player);
        verify(pqSpy,times(1)).remove(player);
        verify(pqSpy,times(1)).add(player);
    }

    @Test
    public void testMethodSetPq() {
        PriorityQueue<Player> pq = new PriorityQueue<>();
        scoreboard.setPq(pq);
        assertEquals(pq, scoreboard.getPq());
    }
}
