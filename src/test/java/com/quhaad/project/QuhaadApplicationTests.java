package com.quhaad.project;

import com.quhaad.project.question.service.QuestionService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = QuestionService.class)
class QuhaadApplicationTests {

    @Test
    void contextLoads() {
    }

}
