package com.quhaad.project.game.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

public class RoomTest {
    @InjectMocks
    private Room room;

    @BeforeEach
    public void setUp() {
        room = new Room("roomId");
    }

    @Test
    public void testMethodGetPlayers() {
        List playerList = new ArrayList<Player>();

        Room roomSpy = spy(room);
        when(roomSpy.getPlayers()).thenReturn(playerList);

        List calledPlayerList = roomSpy.getPlayers();
        assertEquals(calledPlayerList, playerList);
    }

    @Test
    public void testMethodGetQuestionSetId() {
        assertEquals(-1, room.getQuestionSetId());
    }

    @Test
    public void testMethodGetRoomId() {
        assertEquals("roomId", room.getRoomId());
    }

    @Test
    public void testMethodGetRound() {
        assertEquals(0, room.getRound());
    }

    @Test
    public void testMethodSetQuestionIdCanOnlyChangeIdOnceAfterRoomInitialized() {
        room.setQuestionSetId(1);
        assertEquals(1, room.getQuestionSetId());
        room.setQuestionSetId(2);
        assertEquals(1, room.getQuestionSetId());
    }

    @Test
    public void testPlayerIsAddedWhenAddPlayerIsCalled() {
        List playerList = room.getPlayers();
        room.addPlayer(new Player("Player1"));
        assertEquals(1, playerList.size());
    }

    @Test
    public void testMethodCheckIsPlayerInThisRoom() {
        Player player = new Player("Player1");
        assertFalse(room.checkIsPlayerInThisRoom(player));
        room.addPlayer(player);
        assertTrue(room.checkIsPlayerInThisRoom(player));
    }

    @Test
    public void testMethodNextRound() {
        room.nextRound();
        assertEquals(1, room.getRound());
    }

    @Test
    public void testCheckIsTheRoomFull() {
        room.addPlayer(new Player("player1"));
        assertFalse(room.checkIsTheRoomFull());
        room.addPlayer(new Player("player2"));
        assertFalse(room.checkIsTheRoomFull());
        room.addPlayer(new Player("player3"));
        assertFalse(room.checkIsTheRoomFull());
        room.addPlayer(new Player("player4"));
        assertTrue(room.checkIsTheRoomFull());
        room.addPlayer(new Player("player5"));
        assertTrue(room.checkIsTheRoomFull());
    }
}
