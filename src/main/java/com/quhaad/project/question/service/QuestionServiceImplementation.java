package com.quhaad.project.question.service;

import com.quhaad.project.question.core.QuestionSet;
import com.quhaad.project.question.repository.QuestionSetRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuestionServiceImplementation implements QuestionService {

    @Autowired
    private QuestionSetRepository questionSetRepository;

    /**
     * This method is used to find all question set.
     *
     * @return List of question set
     */
    @Override
    public List<QuestionSet> findAllQuestionSet() {
        return questionSetRepository.findAll();
    }

    /**
     * This method is used to find question set by id.
     *
     * @param questionSetId int - Question Set Id
     * @return Found QuestionSet
     */
    @Override
    public QuestionSet findQuestionSet(int questionSetId) {
        return questionSetRepository.findById(questionSetId);
    }

    /**
     * This method is used to create new question set.
     *
     * @param questionSet - QuestionSet
     * @return QuestionSet
     */
    @Override
    public QuestionSet registerQuestionSet(QuestionSet questionSet) {
        return questionSetRepository.save(questionSet);
    }

    /**
     * This method is used to delete a question set by question set id.
     *
     * @param questionSetId int - Question Set Id
     */
    @Override
    public void deleteQuestionSet(int questionSetId) {
        questionSetRepository.deleteById(questionSetId);
    }
}
