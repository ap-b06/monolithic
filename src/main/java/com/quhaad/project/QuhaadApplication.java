package com.quhaad.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuhaadApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuhaadApplication.class, args);
    }

}
