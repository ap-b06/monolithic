package com.quhaad.project.question.core;

import java.util.HashMap;
import java.util.List;
import javax.persistence.Embeddable;

@Embeddable
public class Question {

    private String questionText;

    private String questionMediaUrl;

    private String answer1;

    private String answer2;

    private String answer3;

    private String answer4;

    private String correctAnswer;

    private int timeLimit;

    private int point;

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
        setPoint(questionText.length());
    }

    public String getQuestionMediaUrl() {
        return questionMediaUrl;
    }

    public void setQuestionMediaUrl(String questionMediaUrl) {
        this.questionMediaUrl = questionMediaUrl;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public int getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }

    public String getAnswer1() {
        return answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public boolean checkAnswer(String choice) {
        String chosenAnswer = "";
        switch (choice) {
            case "A":
                chosenAnswer = "A";
                break;
            case "B":
                chosenAnswer = "B";
                break;
            case "C":
                chosenAnswer = "C";
                break;
            case "D":
                chosenAnswer = "D";
                break;
            default:
                break;
        }
        return chosenAnswer.equals(correctAnswer);
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public void decrementPoint() {
        this.point -= 1;
    }
}
