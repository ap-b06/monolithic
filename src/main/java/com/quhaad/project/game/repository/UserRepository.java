package com.quhaad.project.game.repository;

import com.quhaad.project.game.model.Player;
import java.util.HashMap;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {

    private HashMap<String, Player> playerMap = new HashMap<>();

    public void registerPlayer(Player player) {
        getPlayerMap().put(player.getUsername(), player);
    }

    public Player findByUsername(String username) {
        return getPlayerMap().get(username);
    }

    public void deleteByUsername(String username) {
        getPlayerMap().remove(username);
    }

    public boolean checkIfUsernameExist(String username) {
        return getPlayerMap().containsKey(username);
    }

    public HashMap<String, Player> getPlayerMap() {
        return playerMap;
    }
}