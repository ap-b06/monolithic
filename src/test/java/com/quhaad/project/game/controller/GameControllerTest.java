package com.quhaad.project.game.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.quhaad.project.game.model.Room;
import com.quhaad.project.game.repository.RoomRepository;
import com.quhaad.project.game.service.GameService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@WebMvcTest(controllers = GameController.class)
public class GameControllerTest {

    @MockBean
    private GameService gameService;

    @Mock
    private RoomRepository roomRepository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testRoomIdFormWithNotExistRoomItShouldReturnRightResponse() throws Exception {
        MvcResult resultOne = mockMvc.perform(post("/create/room1"))
                .andReturn();
        String contentOne = resultOne.getResponse().getContentAsString();
        assertEquals("NEXT", contentOne);
    }

    @Test
    public void testRoomIdFormWithExistsRoomItShouldReturnRightResponse() throws Exception {
        when(gameService.findRoom("room1")).thenReturn(true);
        MvcResult resultOne = mockMvc.perform(post("/create/room1"))
                .andReturn();
        String contentOne = resultOne.getResponse().getContentAsString();
        assertEquals("ERROR", contentOne);
    }

    @Test
    public void testCheckRoomIdNotExistItShouldReturnRightResponse() throws Exception {
        MvcResult resultOne = mockMvc.perform(get("/check/room1"))
                .andReturn();
        String contentOne = resultOne.getResponse().getContentAsString();
        assertEquals("ROOM DOES NOT EXIST", contentOne);
    }

    @Test
    public void testCheckRoomIdExistsItShouldReturnRightResponse() throws Exception {
        when(gameService.findRoom("room1")).thenReturn(true);
        MvcResult resultOne = mockMvc.perform(get("/check/room1"))
                .andReturn();
        String contentOne = resultOne.getResponse().getContentAsString();
        assertEquals("ROOM EXISTS", contentOne);
    }

    @Test
    public void testWhenNextRoundInSpecificRoomShouldReturnTheRightResponse() throws Exception {
        Room room = new Room("room1");
        when(roomRepository.findByRoomId("room1")).thenReturn(room);
        mockMvc.perform(get("/next-round/room1"))
                .andExpect(handler().methodName("nextRound"));
    }

    @Test
    public void testDeletingRoom() throws Exception {
        mockMvc.perform(delete("/delete/{roomId}", "deleteRoom"))
                .andExpect(handler().methodName("deleteRoomId"))
                .andExpect(status().isOk());
    }
}
