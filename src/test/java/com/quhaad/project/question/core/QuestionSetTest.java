package com.quhaad.project.question.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class QuestionSetTest {
    private QuestionSet questionSet;
    private List<Question> listOfQuestions;

    @BeforeEach
    public void setUp() {
        questionSet = new QuestionSet();
        questionSet.setQuestionSetName("Pengetahuan Umum");

        Question newQuestion = new Question();
        newQuestion.setQuestionText("Siapa presiden Indonesia?");
        newQuestion.setQuestionMediaUrl(null);
        newQuestion.setAnswer1("Jokowi");
        newQuestion.setAnswer2("Jacinda");
        newQuestion.setAnswer3("SBY");
        newQuestion.setAnswer4("Boris");
        newQuestion.setCorrectAnswer("1");
        newQuestion.setTimeLimit(10);

        listOfQuestions = new ArrayList<>();
        listOfQuestions.add(newQuestion);
        questionSet.setListOfQuestions(listOfQuestions);
    }

    @Test
    public void testGetQuestionSetName() {
        String questionSetName = questionSet.getQuestionSetName();
        assertEquals("Pengetahuan Umum", questionSetName);
    }

    @Test
    public void testGetListOfQuestions() {
        List<Question> question = questionSet.getListOfQuestions();
        assertEquals(1, question.size());
    }

    @Test
    public void testGetQuestionSetId() {
        int questionSetId = questionSet.getQuestionSetId();
        assertNotNull(questionSetId);
    }

    @Test
    public void testSetQuestionSetId() {
        int newQuestionSetId = 2;
        questionSet.setQuestionSetId(newQuestionSetId);
        int actualQuestionSetId = questionSet.getQuestionSetId();
        assertEquals(newQuestionSetId, actualQuestionSetId);
    }

    @Test
    public void testSetQuestionSetName() {
        String newQuestionSetName = "Ibukota Negara";
        questionSet.setQuestionSetName(newQuestionSetName);
        String actualQuestionSetName = questionSet.getQuestionSetName();
        assertEquals(newQuestionSetName, actualQuestionSetName);
    }

    @Test
    public void testSetlistOfQuestions() {
        Question newQuestion = new Question();
        newQuestion.setQuestionText("Siapa menteri Keuangan Indonesia?");
        newQuestion.setQuestionMediaUrl(null);
        newQuestion.setAnswer1("Sri Mulyani");
        newQuestion.setAnswer2("Jacinda");
        newQuestion.setAnswer3("SBY");
        newQuestion.setAnswer4("Boris");
        newQuestion.setCorrectAnswer("1");
        newQuestion.setTimeLimit(10);

        listOfQuestions = new ArrayList<>();
        listOfQuestions.add(newQuestion);

        questionSet.setListOfQuestions(listOfQuestions);
        List<Question> actualListOfQuestions = questionSet.getListOfQuestions();
        assertEquals(listOfQuestions, actualListOfQuestions);
        assertEquals(newQuestion, actualListOfQuestions.get(0));
    }
}
