package com.quhaad.project.game.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.quhaad.project.game.model.Room;
import java.util.HashMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

public class RoomRepositoryTest {
    @InjectMocks
    private RoomRepository roomRepository;

    @BeforeEach
    public void setUp() {
        roomRepository = new RoomRepository();
    }

    @Test
    public void testMethodGetRoomRepository() {
        HashMap<String, Room> roomMap = new HashMap<>();
        RoomRepository roomRepositorySpy = spy(roomRepository);

        when(roomRepositorySpy.getRoomMap()).thenReturn(roomMap);
        HashMap<String, Room> calledMap = roomRepositorySpy.getRoomMap();
        assertEquals(roomMap, calledMap);
    }

    @Test
    public void testMethodRegisterRoom() {
        HashMap<String, Room> roomMapSpy = spy(roomRepository.getRoomMap());
        RoomRepository roomRepositorySpy = spy(roomRepository);
        Room room = new Room("roomId");

        when(roomRepositorySpy.getRoomMap()).thenReturn(roomMapSpy);
        roomRepositorySpy.registerRoom(room);
        verify(roomMapSpy, times(1)).put(room.getRoomId(), room);
        assertEquals(1, roomMapSpy.size());
    }

    @Test
    public void testMethodFindByRoomId() {
        HashMap<String, Room> roomMapSpy = spy(roomRepository.getRoomMap());
        RoomRepository roomRepositorySpy = spy(roomRepository);

        when(roomRepositorySpy.getRoomMap()).thenReturn(roomMapSpy);
        roomRepositorySpy.findByRoomId("roomId");
        verify(roomMapSpy, times(1)).get("roomId");
    }

    @Test
    public void testMethodDeleteRoomById() {
        HashMap<String, Room> roomMapSpy = spy(roomRepository.getRoomMap());
        RoomRepository roomRepositorySpy = spy(roomRepository);

        when(roomRepositorySpy.getRoomMap()).thenReturn(roomMapSpy);
        roomRepositorySpy.deleteRoomById("roomId");
        verify(roomMapSpy, times(1)).remove("roomId");
    }
}
