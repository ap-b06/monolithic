package com.quhaad.project.game.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.quhaad.project.game.model.Player;
import java.util.HashMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

public class UserRepositoryTest {

    @InjectMocks
    private UserRepository userRepository;

    @BeforeEach
    public void setUp() {
        userRepository = new UserRepository();
    }

    @Test
    public void testGetPlayerMap() {
        HashMap<String, Player> playerMap = new HashMap<>();
        UserRepository userRepositorySpy = spy(userRepository);

        when(userRepositorySpy.getPlayerMap()).thenReturn(playerMap);
        userRepositorySpy.getPlayerMap();
        assertEquals(playerMap, userRepositorySpy.getPlayerMap());
    }

    @Test
    public void testRegisterPlayer() {
        HashMap<String, Player> playerMapSpy = spy(userRepository.getPlayerMap());
        UserRepository userRepositorySpy = spy(userRepository);
        Player player = new Player("player1");

        when(userRepositorySpy.getPlayerMap()).thenReturn(playerMapSpy);
        userRepositorySpy.registerPlayer(player);
        verify(playerMapSpy, times(1)).put(player.getUsername(), player);
    }

    @Test
    public void testFindByUsername() {
        HashMap<String, Player> playerMapSpy = spy(userRepository.getPlayerMap());
        UserRepository userRepositorySpy = spy(userRepository);

        when(userRepositorySpy.getPlayerMap()).thenReturn(playerMapSpy);
        userRepositorySpy.findByUsername("playerUsername");
        verify(playerMapSpy, times(1)).get("playerUsername");
    }

    @Test
    public void testDeleteByUsername() {
        HashMap<String, Player> playerMapSpy = spy(userRepository.getPlayerMap());
        UserRepository userRepositorySpy = spy(userRepository);

        when(userRepositorySpy.getPlayerMap()).thenReturn(playerMapSpy);
        userRepositorySpy.deleteByUsername("playerUsername");
        verify(playerMapSpy, times(1)).remove("playerUsername");
    }

    @Test
    public void testCheckIfUsernameExist() {
        HashMap<String, Player> playerMapSpy = spy(userRepository.getPlayerMap());
        UserRepository userRepositorySpy = spy(userRepository);

        when(userRepositorySpy.getPlayerMap()).thenReturn(playerMapSpy);
        userRepositorySpy.checkIfUsernameExist("playerUsername");
        verify(playerMapSpy, times(1)).containsKey("playerUsername");
    }
}
