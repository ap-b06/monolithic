package com.quhaad.project.game.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class ChatMessageTest {

    private ChatMessage chatMessage = new ChatMessage();

    @Test
    public void testMethodSetAndGetType() {
        chatMessage.setType(ChatMessage.MessageType.ANSWER);
        assertEquals(ChatMessage.MessageType.ANSWER, chatMessage.getType());
    }

    @Test
    public void testMethodSetAndGetContent() {
        chatMessage.setContent("Content");
        assertEquals("Content", chatMessage.getContent());
    }

    @Test
    public void testMethodSetAndGetSender() {
        chatMessage.setSender("Sender");
        assertEquals("Sender", chatMessage.getSender());
    }

    @Test
    public void testMethodSetAndGetReceiver() {
        chatMessage.setReceiver("Receiver");
        assertEquals("Receiver", chatMessage.getReceiver());
    }
}
