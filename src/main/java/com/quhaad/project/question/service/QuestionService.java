package com.quhaad.project.question.service;

import com.quhaad.project.question.core.QuestionSet;
import java.util.List;

public interface QuestionService {

    public List<QuestionSet> findAllQuestionSet();

    public QuestionSet findQuestionSet(int questionSetId);

    public QuestionSet registerQuestionSet(QuestionSet questionSet);

    public void deleteQuestionSet(int questionSetId);
}